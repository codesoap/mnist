# About
This is my playground for testing neural networks on the
[MNIST](http://yann.lecun.com/exdb/mnist/) data.

I'm using [Keras](https://keras.io/) and convolutional neural networks here.

# Installation
Dependencies I had to install to use this tool are `keras`, `tensorflow`,
`numpy`, `PIL` and `tkinter`. On a Ubuntu machine I was able to install them
like this:

```
sudo pip3 install keras tensorflow numpy PIL
sudo apt install python3-tk
```

Other than that there is no installation procedure. Just clone this repository
and run the tool.

# Usage
In the repository run the tool like this:

```
./cli.py
```

Hopefully the menu you will then see is self explanatory.
