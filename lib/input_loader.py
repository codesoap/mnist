import numpy as np


class MNISTInputError(Exception):
    pass


def get_images(filename):
    test_images_file = open(filename, 'rb')

    magic_number = int.from_bytes(test_images_file.read(4), byteorder='big')
    if magic_number != 2051:
        raise MNISTInputError("Wrong magic number!")

    number_of_images = int.from_bytes(test_images_file.read(4), byteorder='big')
    rows = int.from_bytes(test_images_file.read(4), byteorder='big')
    columns = int.from_bytes(test_images_file.read(4), byteorder='big')

    dt = np.dtype("(1,{},{})>u1".format(rows, columns))
    images = np.fromfile(test_images_file, dtype=dt)

    test_images_file.close()
    return images


def get_labels(filename):
    test_labels_file = open(filename, 'rb')

    magic_number = int.from_bytes(test_labels_file.read(4), byteorder='big')
    if magic_number != 2049:
        raise MNISTInputError("Wrong magic number!")

    number_of_labels = int.from_bytes(test_labels_file.read(4), byteorder='big')

    labels = []
    for _ in range(number_of_labels):
        label_raw = int.from_bytes(test_labels_file.read(1), byteorder='big')
        label = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        label[label_raw] = 1
        labels.append(label)

    test_labels_file.close()
    return np.array(labels, dtype='uint8')
