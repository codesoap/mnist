import lib.input_loader as input_loader


def get_train_data():
    images = input_loader.get_images('data/train-images.idx3-ubyte')
    labels = input_loader.get_labels('data/train-labels.idx1-ubyte')
    return {'x': images, 'y': labels}


def train_neural_network(neural_network, train_data, epochs=10):
    neural_network.fit(
        train_data['x'], train_data['y'],
        epochs=epochs, batch_size=1024, verbose=1
    )
