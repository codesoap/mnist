from PIL import Image, ImageDraw
from tkinter import Tk, Canvas
import lib.input_loader as input_loader
import numpy as np


def get_mnist_test_data():
    test_images = input_loader.get_images('data/t10k-images.idx3-ubyte')
    test_labels = input_loader.get_labels('data/t10k-labels.idx1-ubyte')
    return {'x': test_images, 'y': test_labels}


def print_success_rate(neural_network, test_data):
    correct_answers_count = 0;
    wrong_answers_count = 0;
    predicted_outputs = neural_network.predict(test_data['x'], batch_size=1024)

    for i, correct_output in enumerate(test_data['y']):
        if (correct_output.argmax() == predicted_outputs[i].argmax()):
            correct_answers_count += 1;
        else:
            wrong_answers_count += 1;

    print(
        str(correct_answers_count) + " / " + str(wrong_answers_count)
        + " = correct guesses / wrong guesses"
    )


def print_guesses_for_images(neural_network, images):
    predicted_outputs = neural_network.predict(images)

    for predicted_output in predicted_outputs:
        guess = predicted_output.argmax()
        print("Guessed digit: {} with confidence: {:.2f}".format(
            guess, predicted_output[guess]
        ))


def get_manual_test_image():
    master = Tk()
    master.attributes('-type', 'dialog')
    master.title("Draw Single Digit")

    dummy_canvas = Canvas(master, width=28*4, height=28*4, background="black")
    dummy_canvas.pack()

    image = Image.new("RGB", (28*4, 28*4), "black")
    canvas = ImageDraw.Draw(image)

    dummy_canvas.bind(
        "<B1-Motion>",
        lambda event, c=canvas, d=dummy_canvas: draw_on_canvas(event, c, d)
    )
    master.bind(
        "<Return>",
        lambda event: master.destroy()
    )

    print('Draw your digit end press enter to continue')
    master.mainloop()

    normalized_image = normalize_image(image)
    return np.array([[np.array(normalized_image)]], dtype="uint8")


def draw_on_canvas(event, canvas, dummy_canvas):
   x1, y1 = (event.x - 3), (event.y - 3)
   x2, y2 = (event.x + 3), (event.y + 3)
   dummy_canvas.create_oval(x1, y1, x2, y2, fill="white", outline="white")
   canvas.ellipse((x1, y1, x2, y2), fill="white")


# Normalize like the images in the MNIST database are:
# 1. Crop and scale to 20x20
# 2. Place center of mass within center of 28x28 image
def normalize_image(image):
    normalized_image = image.convert('L')

    bbox = normalized_image.getbbox()
    if bbox:
        normalized_image = normalized_image.crop(bbox)
    normalized_image = normalized_image.resize((20, 20), Image.ANTIALIAS)

    c_x, c_y = get_center_of_mass(normalized_image)

    mnist_image = Image.new('L', (28, 28), 0)
    offset = int(round(4 + 9.5 - c_x)), int(round(4 + 9.5 - c_y))
    mnist_image.paste(normalized_image, offset)

    return mnist_image


def get_center_of_mass(image):
    ar = np.asarray(image)
    x_weights = np.sum(ar, 0)
    y_weights = np.sum(ar, 1)
    com_x =  np.sum(x_weights * np.arange(len(x_weights))) / np.sum(x_weights)
    com_y =  np.sum(y_weights * np.arange(len(y_weights))) / np.sum(y_weights)
    return (com_x, com_y)
