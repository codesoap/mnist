from keras.layers import Conv2D, Dropout, Dense, Flatten
from keras.models import Sequential
import keras


# Topology taken from:
# https://towardsdatascience.com/a-simple-2d-cnn-for-mnist-digit-recognition-a998dbc1e79a
def generate_neural_network():
    neural_network = Sequential()

    neural_network.add(Conv2D(
        input_shape=(1, 28, 28),
        filters=32,
        kernel_size=3,
        strides=(1, 1),
        padding='valid',
        data_format='channels_first',
        dilation_rate=(1, 1),
        activation='relu',
        use_bias=False,
        kernel_initializer='glorot_uniform',
    ))
    neural_network.add(Conv2D(
        filters=64,
        kernel_size=3,
        strides=(1, 1),
        padding='valid',
        data_format='channels_first',
        dilation_rate=(1, 1),
        activation='relu',
        use_bias=False,
        kernel_initializer='glorot_uniform',
    ))
    neural_network.add(Dropout(0.25))
    neural_network.add(Flatten())
    neural_network.add(Dense(units=128, activation='relu'))
    neural_network.add(Dropout(0.5))
    neural_network.add(Dense(units=10, activation='softmax'))
    neural_network.compile(
        loss='categorical_crossentropy',
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy']
    )
    return neural_network
