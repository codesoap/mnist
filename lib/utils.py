from PIL import Image
import gzip
import os
import urllib.request


def display_image(image):
        img = Image.fromarray(image, mode='L')
        img.show()


def provide_mnist_data():
    if not os.path.exists("data"):
        os.makedirs("data")
    for data_file in [
        "data/t10k-labels.idx1-ubyte",
        "data/t10k-images.idx3-ubyte",
        "data/train-labels.idx1-ubyte",
        "data/train-images.idx3-ubyte",
    ]:
        if not os.path.isfile(data_file):
            print("Downloading {} ...".format(data_file))
            download_and_unzip(data_file)


def download_and_unzip(data_file):
    url = "http://yann.lecun.com/exdb/mnist/{}.gz".format(
        os.path.basename(data_file).replace('.', '-')
    )
    with urllib.request.urlopen(url) as response:
        with gzip.GzipFile(fileobj=response) as uncompressed:
            open(data_file, 'wb').write(uncompressed.read())
