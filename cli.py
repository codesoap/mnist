#!/usr/bin/env python3

import lib.neural_network as nn
import lib.training as training
import lib.testing as testing
import lib.utils as utils
import keras
import numpy as np
import os


TRAIN_DATA = None
TEST_DATA = None


def main():
    utils.provide_mnist_data()
    run_mainloop()


def run_mainloop():
    neural_network = None
    neural_network_saved = True
    while True:
        print("[G]enerate new neural network")
        print("[L]oad neural network")
        print("[T]rain neural network")
        print("[A]utomatically test neural network with MNIST test data")
        print("[M]anually test neural network")
        print("[S]ave current neural network")
        print("[Q]uit")
        selection = input("> ")

        if selection.lower() == 'g':
            neural_network = generate_neural_network(neural_network)
        elif selection.lower() == 'l':
            neural_network = load_neural_network()
        elif selection.lower() == 't':
            train_neural_network(neural_network)
            neural_network_saved = False
        elif selection.lower() == 'a':
            auto_test_neural_network(neural_network)
        elif selection.lower() == 'm':
            manually_test_neural_network(neural_network)
        elif selection.lower() == 's':
            save_neural_network(neural_network)
            neural_network_saved = True
        elif selection.lower() == 'q':
            if not neural_network_saved:
                print("The current network is not saved!")
                if input("Quit anyway [y/N]: ").lower() != 'y':
                    continue
            break
        print("")


def generate_neural_network(neural_network):
    print("Generating new neural network...")
    return nn.generate_neural_network()


def load_neural_network():
    nn_file_default = "./saved_networks/latest.h5"
    print("Loading neural network...")
    print("Enter a file to load the neural network from")
    print("Default is {}".format(nn_file_default))
    nn_file = input("File path (empty for default): ") or nn_file_default
    try:
        return keras.models.load_model(nn_file)
    except OSError:
        print("ERROR: Could not load file")
        return None


def train_neural_network(neural_network):
    global TRAIN_DATA
    if neural_network == None:
        print("Please generate or load a neural network first")
    else:
        if not TRAIN_DATA:
            print("Loading training data...")
            TRAIN_DATA = training.get_train_data()
        epochs = int(input("Amount of training epochs: "))
        training.train_neural_network(neural_network, TRAIN_DATA, epochs)


def auto_test_neural_network(neural_network):
    global TEST_DATA
    if neural_network == None:
        print("Please generate or load a neural network first")
    else:
        if not TEST_DATA:
            print("Loading testing data...")
            TEST_DATA = testing.get_mnist_test_data()
        testing.print_success_rate(neural_network, TEST_DATA)


def manually_test_neural_network(neural_network):
    if neural_network == None:
        print("Please generate or load a neural network first")
    else:
        image = testing.get_manual_test_image()
        testing.print_guesses_for_images(neural_network, image)


def save_neural_network(neural_network):
    nn_file_default = "./saved_networks/latest.h5"
    print("Saving neural network...")
    print("Enter a file to save the neural network to")
    print("Default is {}".format(nn_file_default))
    nn_file = input("File path (empty for default): ") or nn_file_default
    nn_dir = os.path.dirname(nn_file)
    if not os.path.exists(nn_dir):
        os.makedirs(nn_dir)
    neural_network.save(nn_file)


if __name__ == '__main__':
    main()
